/* audiosplitFunc.h 
 */
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <string>
#include "AdventCommon.h"

int TEST(const int&, char**, 
        std::ifstream&, std::string&);
int MakeTrack(const std::string&, const std::string&, std::istream&);


inline
int GetExtension(const std::string &s, std::string &a)
{
    std::string::const_iterator beg = s.begin();
    unsigned pos = 0, count = 0, found = 0;

    for (std::string::const_iterator end = s.end(); 
            (beg + count) != end; ++count) {
        char c = *(beg + count);
        if (c == '.') {
            if (found == 0) {
                found = 1;
            }
            pos = count;
        } 
    }

    if (found == 0) {
        std::cerr << "Filename has no extension" << std::endl;
        return -4;
    }

    ++pos;
    for (std::string::const_iterator end = s.end(); 
            (beg + pos) != end; ++pos) {
        a += *(beg + pos);
    }

    return 0;
}

template <typename T>
inline
void PrintSimp(const std::string &s, const T &u)
{
    std::cout << s << " " << u << std::endl;
}

/* 
 * Check for the existence of name
 * https://stackoverflow.com/a/12774387
 * https://en.wikipedia.org/wiki/Stat_(system_call)#stat()_functions
 */
inline 
bool exists_test3 (const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

inline
unsigned GetNum(const std::string::iterator &sit, unsigned &u)
{
    std::string num = "";
    while (true) {
        if (isdigit(*(sit + u))) {
            num += *(sit + u);
            ++u;
        } else {
            ++u;
            break;
        }
    }

    if (num.empty()) {
        return 0;
    } else {
        return std::stoi(num);
    }
}

inline
unsigned Seconds(const unsigned &h, const unsigned &m,
        const unsigned &s)
{
    return (60 * 60 * h + 60 * m + s);
}

inline
unsigned GetSeconds(const std::string::iterator &sit, unsigned &u) 
{
    unsigned hour = 0, min = 0, sec = 0;

    hour = GetNum(sit, u); 
    min = GetNum(sit, u); 
    sec = GetNum(sit, u); 
    
    return Seconds(hour, min, sec); 
}

inline
void GetWord(const std::string::iterator &sit, 
        const std::string::iterator &end, 
        unsigned &u, std::string &word)
{
    word = "";

    while (true) {
        if ((sit + u) != end && !isspace(*(sit + u)) && *(sit + u) != ',') {
            word += *(sit + u);
            ++u;
        } else {
            ++u;
            break;
        }
    }
}

