/* Externally defined functions for audiosplitFunc.h
 */
#include "audiosplitFunc.h"

int TEST(const int &argc, char **argv, 
        std::ifstream &fin, std::string &audioname)
{
    if (argc != 2) {
        std::cerr << "Enter one input filename only" << std::endl;
        return -1;
    }

    fin.open(argv[1]);
    
    if (!fin) {
        std::cerr << "Enter a valid filename" << std::endl;
        return -2;
    }

    std::string line;
    getline(fin, line);
    audioname = line;

    if(!exists_test3(audioname)) {
        std::cerr << "audio file does not exist" << std::endl;
        return -3;
    }

    getline(fin, line);

    return 0;
}

int MakeTrack(const std::string &audioname, 
        const std::string &extension, std::istream &fin)
{
    // Start line by line sequencing
    //std::cout << "line ";
    std::string line;
    getline(fin, line);
    //std::cout << line << std::endl;
    
        // Get seconds at track start
    unsigned count = 0;
    std::string::iterator lineit = line.begin();

    //std::cout << "beginsec ";
    unsigned beginsec = GetSeconds(lineit, count);
    //std::cout << beginsec << std::endl;

        // Get first line that's not a note line
    std::string word;
    //std::cout << "word ";
    std::string::iterator endit = line.end();

    GetWord(lineit, endit, count, word);
    //std::cout << word << std::endl;

    while (word == "Note" || word == "note") {
        //std::cout << "while" << std::endl;
        getline(fin, line);
        lineit = line.begin();
        endit = line.end();
        count = 0;

        GetSeconds(lineit, count);
        GetWord(lineit, endit, count, word);
    }

        // Build the track name
    //std::cout << "name ";
    std::string name = word;
    //std::cout << name << std::endl;

    while ((lineit + count) < endit) {
        GetWord(lineit, endit, count, word);
        if (word != "begins" && !word.empty()) {
            if (word != "-" && name[name.size() - 1] != '-') {
                name += "_";
            }
            name += word; 
    //std::cout << name << std::endl;
        }
    }

        // Get end seconds
    getline(fin, line);
    lineit = line.begin();
    endit = line.end();
    count = 0;
    unsigned endsec = 0;

    if (!line.empty()) {
        endsec = GetSeconds(lineit, count);
        
    // ffmpeg -ss begsec -to endsec -i input.mp3 output.mp3 
    // start at 10 stop at 16 put in op
        std::string ffmpeg = "ffmpeg -ss " 
            + std::to_string(beginsec) + " -to " 
            + std::to_string(endsec) + " -i " + audioname 
            + " " + name + "." + extension;

        //std::cout << ffmpeg << std::endl;
        const char *command = ffmpeg.c_str();
        system(command);
    }

    return 0;
}
