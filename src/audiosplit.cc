/* audiosplit.cc
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "audiosplitFunc.h"

int main(int argc, char **argv)
{
    // Check input, init fstream
    std::ifstream fin;
    std::string audioname;

    int testret = TEST(argc, argv, fin, audioname);
    if (testret != 0) {
        return testret;
    }

    // Find extension
    std::string extension = "";
    
    testret = GetExtension(audioname, extension);
    if (testret != 0) {
        return testret;
    }
    
    for (unsigned c = 0; c < 15; ++c) {
        std::cout << c + 1 << std::endl;
        MakeTrack(audioname, extension, fin);
    }
}
