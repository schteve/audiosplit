/* AdventCommon.h 
 */

#ifndef ADVENTCOMMON_H
#define ADVENTCOMMON_H

#include <iostream>
#include <sstream>
#include <string>

template <typename T> inline void Result(const std::string &mess, const T &ret)
{
    std::cout << mess << std::endl
        << ret << std::endl;
}

inline
void GetNum(std::istream& is, unsigned &a)
{
    char c;
    std::string num = "";

    while (is >> c) {
        if (isdigit(c)) {
            num += c;
        } else if (!num.empty()) {
            break;
        }
    }
    a = std::stoi(num);
}

inline
unsigned GetNum(std::istream& is)
{
    char c;
    std::string num = "";

    while (is >> c) {
        std::cerr << c << "|";
        if (isdigit(c) && c != ' ') {
            num += c;
        } else {
            break;
        }
    }
    std::cout << std::endl;
    if (!num.empty()) {
        return std::stoi(num);
    } else {
        return 0;
    }
}

inline
long GetNum2(std::string s)
{
    char c;
    std::string num = "";
    std::istringstream is(s);

    while (is >> c) {
        if (isdigit(c)) {
            num += c;
        } else if (!num.empty()) {
            ;
        }
    }
    if (!num.empty()) {
        return std::stol(num);
    } else {
        return 0;
    }
}

#endif
