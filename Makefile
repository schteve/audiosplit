# MAKEFILE FOR simple C++ programming
# orginally by David MacKay

# if you want the debugger kdbg to work nicely, REMOVE the -O2 flag
# if you want to get all warnings enabled, INCLUDE the -O2 flag

CFLAGS = $(INCDIRS)  \
	-pedantic -g -O2\
	-Wall -Wconversion\
	-Wformat  -Wshadow\
	-Wpointer-arith -Wcast-qual -Wwrite-strings\
	-D__USE_FIXED_PROTOTYPES__

LIBS = -l stdc++ -lm

CXX = g++

# audiosplit
audiosplitobjects = bin/audiosplit.o bin/audiosplitFunc.o bin/AdventCommon.o

audiosplit: $(audiosplitobjects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/audiosplit $(audiosplitobjects)

bin/audiosplit.o: src/audiosplit.cc src/audiosplitFunc.h src/AdventCommon.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/audiosplit.cc -o bin/audiosplit.o

bin/audiosplitFunc.o: src/audiosplitFunc.cc src/audiosplitFunc.h src/AdventCommon.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/audiosplitFunc.cc -o bin/audiosplitFunc.o

# Classes
bin/AdventCommon.o: src/AdventCommon.cc src/AdventCommon.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/AdventCommon.cc -o bin/AdventCommon.o

# Clean
clean: 
	rm bin/*.o
