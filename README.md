# audiosplit 
    audiosplit infile

audiosplit splits an audio file specified by the input file using ffmpeg.

input file:

    filename
    00:00:00 start
    ...
    00:0X:YZ - item 1 begins
    00:0x:yz - item 1 ends
    ...
    ..:..:.. - last ``item'' begins
